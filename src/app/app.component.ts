import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  inputLabel = 'Input Pattern:';
  outputLabel = 'Output:';
  invalidPatterntext = 'Please enter a valid expression';
  flag;
  invalidflag;
  stack = [];
  finalStack = [];
  updatedStack = [];
  arithmeticOperations = ['+', '-', '*', '/', '%'];
  calc(inputPattern) {
    if (inputPattern) {
      this.stack = [];
      this.updatedStack = [];
      this.finalStack = [];
      this.flag = false;
      this.invalidflag = false;
      const iArr = inputPattern.split('');
      let pArr;
      if (iArr.length > 1) {
        pArr = inputPattern.split(' ');
        pArr.forEach(element => {
          if (!this.invalidflag) {
            if (this.arithmeticOperations.indexOf(element) === -1) {
              const preSign = element.split('');
              if (isNaN(Number(element)) || this.arithmeticOperations.indexOf(preSign[0]) > -1) {
                // not a valid expression
                this.invalidflag = true;
                this.flag = false;
                this.stack = [];
                this.updatedStack = [];
                this.finalStack = [];
              } else {
                if (element !== '') {
                  this.flag = true;
                  this.stack.unshift(element); // 2 5
                }
                // for (const val of this.stack) {
                //   this.finalStack.unshift(val);
                // }
              }
            } else {// 1 2 + 2 5
              if (this.stack.length) {
                let x;
                let y;
                if (this.stack.length >= 2) {
                  x = Number(this.stack[1]); // 2
                  y = Number(this.stack[0]); // 3
                  // 4 1 2 3 + 7
                  // this.stack = [3, 2, 1, 4] +
                  let i = 2;
                  while (i <= this.stack.length - 1) {
                    this.updatedStack.push(this.stack[i]); // 1, 4
                    i++;
                  }
                } else if (this.updatedStack.length === 0) {
                  this.invalidflag = true;
                  this.flag = false;
                  this.stack = [];
                  this.updatedStack = [];
                  this.finalStack = [];
                } else {
                  x = Number(this.updatedStack[0]);
                  y = Number(this.stack[0]);
                  this.updatedStack = [];
                }
                if (x && y) {
                  switch (element) {
                    case '+':
                      this.updatedStack.unshift(x + y); // 5, 1, 4
                      break;
                    case '-':
                      this.updatedStack.unshift(x - y);
                      break;
                    case '/':
                      this.updatedStack.unshift(x / y);
                      break;
                    case '*':
                      this.updatedStack.unshift(x * y);
                      break;
                    case '%':
                      this.updatedStack.unshift(x % y);
                      break;
                    default:
                      break;
                  }
                  // this.finalStack = this.updatedStack; // 5, 1, 4
                  this.stack = [];
                  this.flag = true;
                }
              } else {
                // consecutive arithmetic symbol
                if (this.updatedStack.length >= 2) {
                  switch (element) {
                    case '+':
                      this.updatedStack.unshift(this.updatedStack[1] + this.updatedStack[0]);
                      this.updatedStack = this.popStack(this.updatedStack);
                      break;
                    case '-':
                      this.updatedStack.unshift(this.updatedStack[1] - this.updatedStack[0]);
                      this.updatedStack = this.popStack(this.updatedStack);
                      break;
                    case '/':
                      this.updatedStack.unshift(this.updatedStack[1] / this.updatedStack[0]);
                      this.updatedStack = this.popStack(this.updatedStack);
                      break;
                    case '*':
                      this.updatedStack.unshift(this.updatedStack[1] * this.updatedStack[0]);
                      this.updatedStack = this.popStack(this.updatedStack);
                      break;
                    case '%':
                      this.updatedStack.unshift(this.updatedStack[1] % this.updatedStack[0]);
                      this.updatedStack = this.popStack(this.updatedStack);
                      break;
                    default:
                      break;
                  }
                  this.finalStack = this.updatedStack;
                  this.flag = true;
                } else {
                  this.invalidflag = true;
                  this.flag = false;
                  this.stack = [];
                  this.updatedStack = [];
                  this.finalStack = [];
                }
              }
            }
          } else {
            // invalid expression
            this.invalidflag = true;
            this.flag = false;
            this.stack = [];
            this.updatedStack = [];
            this.finalStack = [];
          }
        });
        this.finalStack = this.stack.concat(this.updatedStack); // 2 , 5, 3
      } else {
        this.flag = true;
        this.stack.push(iArr[0]);
        this.finalStack = this.stack;
      }
    } else {
      this.flag = false;
      this.finalStack = [];
    }
  }

  popStack(arrToShift): string[] {
    while (arrToShift.length > 1) {
      arrToShift.pop();
    }
    return arrToShift;
  }
  clearStack() {
    this.finalStack = [];
  }
  dropStack() {
    this.finalStack.shift();
  }
  swapStack() {
    if (this.finalStack.length >= 2) {
      const swapValue = this.finalStack[0];
      this.finalStack[0] = this.finalStack[1];
      this.finalStack[1] = swapValue;
    }
  }
  rollStack() {
    if (this.finalStack.length) {
      const revArr = this.finalStack.reverse();
      this.finalStack = revArr;
    }
  }
}
